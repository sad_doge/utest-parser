<?php

namespace App\Services\ParserService\Parsers;

use App\Services\ParserService\ProcessParser;
use App\Services\ParserService\WordFileReader;

class KazGUTestParser extends BaseTextParser implements ProcessParser
{
    private $wordReader;
    public $keys = [
        'A' => 0,
        'B' => 1,
        'C' => 2,
        'D' => 3,
        'E' => 4,
        'А' => 0,
        'В' => 1,
        'С' => 2,
        'Д' => 3,
        'Е' => 4,
        ];

    public function __construct() {
        $this->wordReader = new WordFileReader();
    }



    protected function appendAnswer(string $content, &$answers) {
//        if(str_contains($content, 'тіске металл штампталған сауытты өлшеу'))
//            dd(trim($content));
        $right = str_contains(substr($content, 0, 5), '+');
        $filter = array("[right]", "+","A)", "B)", "C)", "D)", "E)", "A.", "B.", "C.", "D.", "E)", "А.", "Б.", "Г.", "В.", "С.", "Д.", "А)", "Б)", "В)", "С)", "Д)", "Г)", "Е)", "а)", "б)", "в)", "г)", "д)");
        $content = str_replace('«', '"', $content);
        $content = str_replace('»', '"', $content);
        $content = htmlspecialchars(trim(str_replace($filter, "", $content), " \t\n\r\0\x0B"));
        $content = str_replace('&amp;quot;', '&quot;', $content);

        $content = preg_replace('/^' . preg_quote('-', '/') . '/', "", $content);
        $content = preg_replace('/^' . preg_quote('*+', '/') . '/', "", $content);
        $content = preg_replace('/^' . preg_quote('*', '/') . '/', "", $content);
        $answers[] = array(
            'content'  => $content,
            'is_right' => $right,
        );
    }

    protected function setRightAnswer($key, &$answers) {
//        dd(($answers));
        $answers[$this->keys[$key]]['is_right'] = true;
    }

    public function process($file, $dir = null)
    {
        $text = $this->wordReader->readDocxPhpWord($file, $dir);
//        return $text;
//        |^[#][0-9]+   '/^[*]/'   [ABCDEАБВГСДЕ][.]|[ABCDEАБВГСДЕ][)]|
        $text .= "\n999."; // todo fix
        $text .= "\nГрамматика\n should fix it";
        $result = $this->parseText($text, '/^[0-9]+[.]|^[0-9]+[)]|[#][0-9]+|^[$][$][$][0-9][0-9][0-9]/', '/[ABCDEАБВГСДЕ][.]|[ABCDEАБВГСДЕабвгд][)]|[+-]/');
        return  $result;
        return collect($result)->count();
    }
}
