<?php

namespace App\Services\ParserService\Parsers;

use App\Services\ParserService\ProcessParser;
use App\Services\ParserService\WordFileReader;

class SplitAnswerParser implements ProcessParser
{
    private $wordReader;

    public function __construct() {
        $this->wordReader = new WordFileReader();
    }

    public $keys = [
        'A' => 0,
        'B' => 1,
        'C' => 2,
        'D' => 3,
        'E' => 4,
        'А' => 0,
        'В' => 1,
        'С' => 2,
        'Д' => 3,
        'Е' => 4,
    ];

    public function process($file, $dir = null)
    {
        $text = $this->wordReader->readDocxPhpWord($file, $dir);
//        return $text;
//        |^[#][0-9]+   '/^[*]/'
        $text .= "\n999."; // todo fix
        $text .= "\nГрамматика\n should fix it";
    }
}
