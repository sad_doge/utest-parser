<?php

namespace App\Services\ParserService\Parsers;

abstract class BaseTextParser
{
    abstract protected function appendAnswer(string $content, &$answers);

    abstract protected function setRightAnswer($key, &$answers);

    private function contains($string): bool
    {
        $array = [
            'LISTENING SECTION',
            'READING SECTION',
            'Look at the visual',
            'GRAMMAR SECTION',

            'Тыңдалым',
            'Оқылым',
            'Грамматика',

            'Слушание',
            'Чтение',
            'РАССМОТРИ ТАБЛИЦУ',
            'Грамматика',
        ];
        foreach($array as $item) {
            if (stripos($string, $item) !== false)
                return true;
        }
        return false;
    }

    protected function parseText($text, $question_filter, $answer_filter): array
    {
        $question_content = '';
        $answer_content   = '';
        $text_content    = '';

        $question_going = false;
        $answer_going   = false;
        $text_going     = false;

        $result = [
//            'questions' => array()
        ];
        $answers = array();
        $level = '';
        $i = 0;
        foreach(preg_split("/((\r?\n)|(\r\n?))/", $text) as $line){
//            $line = trim($line);
            $i++;
//            dd($line);
//            if($i ==10)
//                dd(preg_match($question_filter, $line));
//                dd(preg_match($answer_filter, $line) && !empty($question_content));
//                dd(preg_match('/[{]Правильный ответ} = /', $line));
//            dd(preg_match($question_filter, $line));
            if (preg_match($question_filter, $line)) {
                if ($question_content != '' && !empty($answers)) {
                    if ($answer_content != '') {
                        $this->appendAnswer($answer_content, $answers);
                    }
                    $question_content = str_replace('«', '"', $question_content);
                    $question_content = str_replace('»', '"', $question_content);
                    $question_content = htmlspecialchars(trim(preg_replace($question_filter, '', $question_content), "   \t\n\r\0\x0B"));
                    $question_content = str_replace('&amp;quot;', '&quot;', $question_content);
                    $question = [
                        'content' => $question_content,
                        'level'   => $level != '' ? $level : null,
                        'answers' => $answers,
                    ];
//                    $result['questions'][] = $question;
                    $result[] = $question;
                    $answer_content = '';
                    $answers = array();
//                    dd($question);
                }

                $question_content   = $line;
                $question_going     = true;
                $answer_going       = false;
                $text_going         = false;
                continue;
            }
//            elseif ($this->contains(trim($line))) {
//                if($text_content != '') {
//                    $result['texts'][] = [
//                        'content' => $text_content
//                    ];
//                }
//
//                $text_content = $line;
//                $question_going     = false;
//                $answer_going       = false;
//                $text_going         = true;
//                continue;
//            }
            elseif (preg_match($answer_filter, $line) && !empty($question_content)) {
                if ($answer_content != '') {
                    $this->appendAnswer($answer_content, $answers);
                }
                $answer_content     = $line;
                $question_going     = false;
                $answer_going       = true;
                $text_going         = false;
                continue;
            } elseif (preg_match('/[{]Дұрыс жауап}/', $line)
                ||  preg_match('/[{]дұрыс жауап}/', $line)
                ||  preg_match('/[{]Правильный ответ}/', $line)
            ) {
                if ($answer_content != '') {
                    $this->appendAnswer($answer_content, $answers);
                    $answer_content = '';
                    $answer_going = false;
                }
                $key = trim(preg_replace('/[{]Дұрыс жауап}/', '', $line));
                $key = trim(preg_replace('/[{]Правильный ответ}/', '', $key));
                $key = trim(preg_replace('/[{]дұрыс жауап}/', '', $key));
                $key = trim(preg_replace('/[=]/', '', $key));
                $this->setRightAnswer($key, $answers);
                continue;
            }
            elseif (preg_match('/[{]Күрделілігі}/', $line)
                ||  preg_match('/[{]сложность}/', $line)
                || preg_match('/[{]күрделілігі}/', $line)) {
                $level = trim(preg_replace('/[{]Күрделілігі}/', '', $line));
                $level = trim(preg_replace('/[{]сложность}/', '', $level));
                $level = trim(preg_replace('/[{]күрделілігі}/', '', $level));
                $level = trim(preg_replace('/[=]/', '', $level));
            }


            if ($question_going) {
                $question_content   .= ' '. $line;
            }
            elseif ($answer_going) {
                $answer_content     .= $line;
            }
            elseif ($text_going) {
                $text_content       .= $line;
            }
        }

        return $result;
    }
}
