<?php

namespace App\Services\ParserService\Parsers;

use App\Services\ParserService\ProcessParser;
use Carbon\Carbon;

class DarkanDalaParser implements ProcessParser
{
    private  $months = [
        'января' => '01',
        'февраля' => '02',
        'марта' => '03',
        'апреля' => '04',
        'мая' => '05',
        'июня' => '06',
        'июля' => '07',
        'августа'=> '08',
        'сентября' => '09',
        'октября' => '10',
        'ноября' => '11',
        'декабря' => '12',
    ];

    private function getTimeStamp($date) {
        return Carbon::parse(trim($date, ' г.'))->timestamp;
    }

    private function getTimeStampArray(array $array_dates) {
        return isset($array_dates[1]) ? $this->getTimeStamp($array_dates[1]) : $this->getTimeStamp($array_dates[0]);
    }

    private function parseGround($file) {
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
//        $highest = $spreadsheet->getActiveSheet()->getHighestRow('A') - 17;
        $protocol = explode("от", $spreadsheet->getActiveSheet()->getCell('G11')->getValue());

        $selection_date = explode('-', $spreadsheet->getActiveSheet()->getCell('F18')->getValue());
        $analyze_date = explode('-', $spreadsheet->getActiveSheet()->getCell('F24')->getValue());
        $come_date = explode('-', $spreadsheet->getActiveSheet()->getCell('F19')->getValue());

        $result = array(
            'name' => trim($protocol[0], '№ '),
            'protocol_date'  => $protocol[1] ?? null,
            'come_date_start' =>  $this->getTimeStamp($come_date[0]),
            'come_date_end' => $this->getTimeStampArray($come_date),
            'analyze_date_start' => $this->getTimeStamp($analyze_date[0]),
            'analyze_date_end' => $this->getTimeStampArray($analyze_date),
            'selection_date_start' => $this->getTimeStamp($selection_date[0]),
            'selection_date_end' => $this->getTimeStampArray($selection_date),
            'indicators' => array(),
        );

        $name_num = str_contains($spreadsheet->getActiveSheet()->getCell('F26')->getValue(), 'Наименование') ? 27 : 26;

        foreach (range('F', 'K') as $column) {
            $name_unit = $spreadsheet->getActiveSheet()->getCell($column . $name_num)->getValue();

            $name_unit = explode(",", $name_unit);


            $indicator = [
                'name' => trim($name_unit[0]),
                'unit' => trim($name_unit[1]) ?? null,
                'places' => array(),
            ];
            $field = null;
            foreach (range(31, ( $spreadsheet->getActiveSheet()->getHighestRow('A') )) as $row) {

                if (str_contains( $spreadsheet->getActiveSheet()->getCell('A' . $row)->getValue(), 'Примечание: ИНО')) {
                    break;
                }

                $field = $spreadsheet->getActiveSheet()->getCell('C' . $row)->getValue() ?: $field;
                $indicator['places'][] = [
                    'id' => $spreadsheet->getActiveSheet()->getCell('B' . $row)->getValue(),
                    'grid' => $spreadsheet->getActiveSheet()->getCell('D' . $row)->getValue(),
                    'field' => $field,
                    'value' => $spreadsheet->getActiveSheet()->getCell($column . $row)->getValue(),
                ];
            }
            $result['indicators'][] = $indicator;
        }

        return $result;
    }


    public function process($file) {
        return $this->parseGround($file);
    }
}
