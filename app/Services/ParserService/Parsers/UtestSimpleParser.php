<?php

namespace App\Services\ParserService\Parsers;

use App\Services\ParserService\ProcessParser;
use App\Services\ParserService\WordFileReader;

class UtestSimpleParser extends BaseTextParser implements ProcessParser
{
    private $wordReader;

    public function __construct() {
        $this->wordReader = new WordFileReader();
    }

    protected function appendAnswer(string $content, &$answers) {
        $answers[] = array(
            'content'  => trim($content, '@ #'),
            'is_right' => empty($answers),
        );
    }

    public function process($file, $dir = null, $ext = null) {
        switch ($ext) {
            case ($ext == 'doc') :
                $text = $this->wordReader->readDoc($file);
                break;
            case ($ext == 'docx') :
                $text = $this->wordReader->readDocxPhpWord($file, $dir);
                break;
            default :
                return  response()->json([
                    'error' => 'Could not parse a file: ' . $ext
                ]);
        }
        $text .= "\n@ asdf";  // todo fix
        return $this->parseText($text, '/^[@]/', '/[#]/');
    }

    protected function setRightAnswer(string $content, &$answers)
    {
        // TODO: Implement setRightAnswer() method.
    }
}
