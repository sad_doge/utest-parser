<?php

namespace App\Services\ParserService\Parsers;

use App\Services\ParserService\ProcessParser;

class ExcelTestParser implements ProcessParser
{

    public function process($file)
    {
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
        $result = [];
        foreach ($spreadsheet->getAllSheets() as $sheet) {

            foreach (range(2, $sheet->getHighestRow('B')) as $i) {
                $act = $sheet->getCell('B' . $i)->getValue();
                if(is_null($act))
                    continue;

                foreach (range('D', 'G') as $column) {
                    $answers[] = [
                        'content' => $sheet->getCell($column  . $i)->getValue(),
                        'is_right' => $column == 'D',
                    ];
                }

                $question = [
                    'act'     => $act,
                    'content' => $sheet->getCell('C' . $i)->getValue(),
                    'answers' => $answers,
                ];
                $answers = [];
                $result[] = $question;
            }
        }
        $dir = time();

        $result = collect($result)->groupBy('act');
//        return $result;
        $final = [];
        foreach ($result as $key => $value) {
//            dd(get_class($value));
            $final[] = [
                'act' => $key,
                'questions' => $value->map(function ($item) {
                    return $item;
                })
            ];
        }
        return  $final;

        if (!is_dir(storage_path('app/public/' . $dir))) {
            mkdir(storage_path('app/public/' . $dir));
        }

        file_put_contents(
            storage_path('app/public/'. $dir . '/'. 'response.json'),
                json_encode($result)
        );

        return (asset('/storage/'. $dir . '/' . 'response.json'));

    }
}
