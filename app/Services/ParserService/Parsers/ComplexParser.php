<?php

namespace App\Services\ParserService\Parsers;

use App\Services\ParserService\ProcessParser;
use App\Services\ParserService\WordFileReader;

class ComplexParser extends BaseTextParser implements ProcessParser
{
    private $wordReader;

    public function __construct() {
        $this->wordReader = new WordFileReader();
    }

    protected function appendAnswer(string $content, &$answers) {
        $filter = array("[right]", "A)", "B)", "C)", "D)", "A.", "B.", "C.", "D.", "А.", "В.", "С.", "Д.", "А)", "В)", "С)", "Д)");
        $answers[] = array(
            'content'  => trim(str_replace($filter, "", $content)),
            'is_right' => str_contains($content, '[right]'),
        );
    }

    public function process($file, $dir = null): array
    {
        $text = $this->wordReader->readDocxPhpWord($file, $dir);
        $text .= "\n999."; // todo fix
        $text .= "\nГрамматика\n should fix it";
        return $this->parseText($text, '/^[0-9]+[.]|^[0-9]+[)]/', '/[ABCDАВСД][.]|[ABCDАВСД][)]/');
    }

    protected function setRightAnswer(string $content, &$answers)
    {
        // TODO: Implement setRightAnswer() method.
    }
}
