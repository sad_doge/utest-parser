<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use DOMDocument;
use DOMXPath;
use Illuminate\Http\Request;

class QuillController extends Controller
{
    private $types = ['jpg', 'jpeg', 'gif', 'png'];

    public function index() {
        return view('quill');
    }

    public function store(Request $request) {
        $dir = time();
        if (!is_dir(storage_path('app/public/' . $dir))) {
            mkdir(storage_path('app/public/' . $dir));
        }

        $doc = new DOMDocument();
        $doc->loadHTML($request->input('text'));
        $items = $doc->getElementsByTagName('img');
        if(count($items) > 0) {
            foreach ($items as $item) {
                $base64 = $item->getAttribute('src');
                $extension = substr($base64, strpos($base64, 'data:image/') + 11, strpos($base64, ';base64') - 11);
                if (preg_match('/^data:image\/(\w+);base64,/', $base64)) {
                    if (!in_array($extension, $this->types)) {
                        throw new \Exception('invalid image type: ' . $extension . ' Available extensions: ' . implode("|",$this->types));
                    }
                }
                $name = uniqid(). '.' . $extension;
                file_put_contents(
                    storage_path('app/public/'. $dir . '/'. $name),
                    base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64))
                );
                $item->setAttribute('src', asset('/storage/'. $dir . '/' . $name));
            }
        }
        $result = $doc->saveHTML();
//        return $result;
        dd( $result);
    }
}
