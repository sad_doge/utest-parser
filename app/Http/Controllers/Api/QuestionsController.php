<?php

namespace App\Http\Controllers\Api;

use App\Domain\Contracts\ParserTypeContract;
use App\Http\Controllers\Controller;
use App\Services\ParserService\Parsers\ComplexParser;
use App\Services\ParserService\Parsers\DarkanDalaParser;
use App\Services\ParserService\Parsers\ExcelTestParser;
use App\Services\ParserService\Parsers\KazGUTestParser;
use App\Services\ParserService\Parsers\UtestSimpleParser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QuestionsController extends Controller
{
    public function parseTypes() {
        return array(
            [
                'id' => ParserTypeContract::UTEST_PARSER_ID,
                'name' => 'UTEST: @ - вопрос # - ответ'
            ],
            [
                'id' => ParserTypeContract::DARKANDALA_PARSER_ID,
                'name' => 'Darkandala: ПРОТОКОЛ ИСПЫТАНИЯ ПОЧВЫ '
            ],
            [
                'id' => ParserTypeContract::OLYMPIC_TEST_PARSER_ID,
                'name' => 'Олимп тесты'
            ],
            [
                'id' => ParserTypeContract::EXCEL_TEST_PARSER_ID,
                'name' => 'Excel test'
            ],
            [
                'id' => ParserTypeContract::KAZGU_TEST_PARSER_ID,
                'name' => 'KazGU test parser'
            ],
        );
    }

    public function parse(Request $request) {
        $error = Validator::make($request->all(), array(
            'parser_type_id' => ['required'],
            'file' => ['required']
        ));

        if($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }
        $additional_files = [];

        if (is_array($request->file('file')))
            foreach ($request->file('file') as $file) {
                if(in_array($file->extension(), ['doc', 'docx', 'xlsx']))
                    $parser_file = $file;
                else
                    $additional_files[] = $file;
            }
        elseif (in_array($request->file('file')->extension(), ['doc', 'docx', 'xlsx']))
            $parser_file = $request->file('file');
        else
            return  response()->json([
                'error' => 'Parser file not found'
            ]);


        $type = $request->input('parser_type_id');

        if($type == ParserTypeContract::UTEST_PARSER_ID) {
            $parser = new UtestSimpleParser();
        }
        elseif ($type == ParserTypeContract::OLYMPIC_TEST_PARSER_ID) {
            $parser = new ComplexParser();
        }
        elseif ($type == ParserTypeContract::DARKANDALA_PARSER_ID) {
            $parser = new DarkanDalaParser();
        }
        elseif ($type == ParserTypeContract::EXCEL_TEST_PARSER_ID) {
            $parser = new ExcelTestParser();
        }
        elseif ($type == ParserTypeContract::KAZGU_TEST_PARSER_ID) {
            $parser = new KazGUTestParser();
        }
        else {
            return  response()->json([
                'error' => 'Unknown parser type: ' . $parser_file->extension()
            ]);
        }

        $dir = time();

        $result =  $parser->process($parser_file, $dir, $parser_file->extension());

        if($additional_files) {
            foreach ($additional_files as $file) {
                $name = $file->store('public/'. $dir);
                $name = str_replace('public/', '', $name);
                $result['additional_files'][] = asset('/storage/' . $name);
            }
        }


        return response()->json($result);
    }
}
