<?php

use App\Http\Controllers\Web\QuillController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/quill', [QuillController::class, 'index'])->name('quill.index');

Route::post('/quill', [QuillController::class, 'store'])->name('quill.store');
