git clone {URL REPO}

После нужно ввести следующие команды

copy .env.example .env 

docker run --rm -v $(pwd):/opt -w /opt laravelsail/php80-composer:latest composer install --ignore-platform-reqs

vendor/bin/sail artisan key:generate

vendor/bin/sail artisan storage:link // для картинок

После запускаем докер

docker-compose up -d

или 

vendor/bin/sail up -d

Типы парсера:
   1) Парсер для простого формата где есть @ - вопрос # - ответы 
   2) ДарханДала анализ грунта (.xlsx)
   3) Сложный парсер для олимп тестов

API:

    GET: http://localhost/api/types
        RESPONSE: all available parser types
        

    POST: http://localhost/api/parse
        BODY:   file[] -> file   // можно закинуть доп. файлы тип аудио, 
                parser_type_id -> id of parser types

если есть картинки, они будут в контенте виде ссылки 
![img.png](img.png)


доп. файлы
![img_1.png](img_1.png)
